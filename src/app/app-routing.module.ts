import { NgModule           }   from    '@angular/core';
import { RouterModule       }   from    '@angular/router';
import { Routes             }   from    '@angular/router';

const routes: Routes            =   [{ 
    path                        :   '', 
    loadChildren                :   './tabs/tabs.module#TabsPageModule'
    /*
}, { 
    path                        :   'event/:id', 
    loadChildren                :   './event/event.module#EventModule'
    */
}, { 
    path                        :   'ngo/:id', 
    loadChildren                :   './ngo/ngo.module#NgoModule'
}, { 
    path                        :   'volunteer/:id', 
    loadChildren                :   './volunteer/volunteer.module#VolunteerModule'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, { enableTracing: true })],
    exports: [RouterModule]
})
export class AppRoutingModule {}
