import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgoUpcomingPage } from './upcoming.page';

describe('NgoUpcomingPage', () => {
  let component: NgoUpcomingPage;
  let fixture: ComponentFixture<NgoUpcomingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgoUpcomingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgoUpcomingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
