import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NgoUpcomingPage } from './upcoming.page';

const routes: Routes = [
  {
    path: '',
    component: NgoUpcomingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NgoUpcomingPage]
})
export class NgoUpcomingPageModule {}
