import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NgoVolunteerPage } from './volunteer.page';

const routes: Routes = [
  {
    path: '',
    component: NgoVolunteerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NgoVolunteerPage]
})
export class NgoVolunteerPageModule {}
