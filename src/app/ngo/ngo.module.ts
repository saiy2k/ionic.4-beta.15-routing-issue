import { NgModule           }   from    '@angular/core';
import { CommonModule       }   from    '@angular/common';
import { RouterModule       }   from    '@angular/router';

const routes = [{
    path                        :   'home',
    loadChildren                :   './home/home.module#NgoHomePageModule'
    /*
}, {
    path                        :   'upcoming',
    loadChildren                :   './upcoming/upcoming.module#NgoUpcomingPageModule'
}, {
    path                        :   'past',
    loadChildren                :   './past/past.module#NgoPastPageModule'
}, {
    path                        :   'volunteer',
    loadChildren                :   './volunteer/volunteer.module#NgoVolunteerPageModule'
    */
}, {
    path                        :   '',
    redirectTo                  :   'home'
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule
    ],
    declarations: []
})
export class NgoModule { }
