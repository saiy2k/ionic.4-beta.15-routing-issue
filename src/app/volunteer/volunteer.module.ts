import { NgModule           }   from    '@angular/core';
import { CommonModule       }   from    '@angular/common';
import { RouterModule       }   from    '@angular/router';

const routes = [{
    path                        :   'home',
    loadChildren                :   './home/home.module#VolunteerHomePageModule'
}, {
    path                        :   'event',
    loadChildren                :   './event/event.module#VolunteerEventPageModule'
}, {
    path                        :   'achievement',
    loadChildren                :   './achievement/achievement.module#VolunteerAchievementPageModule'
}, {
    path                        :   '',
    redirectTo                  :   'home'
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule
    ],
    declarations: []
})
export class VolunteerModule { }

