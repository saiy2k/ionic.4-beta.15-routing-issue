import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventVolunteerPage } from './volunteer.page';

const routes: Routes = [
  {
    path: '',
    component: EventVolunteerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventVolunteerPage]
})
export class EventVolunteerPageModule {}
