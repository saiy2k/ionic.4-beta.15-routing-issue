import { NgModule           }   from    '@angular/core';
import { CommonModule       }   from    '@angular/common';
import { RouterModule       }   from    '@angular/router';

const routes = [{
    path                        :   'home',
    loadChildren                :   './home/home.module#EventHomePageModule'
}, {
    path                        :   'album',
    loadChildren                :   './album/album.module#EventAlbumPageModule'
}, {
    path                        :   'review',
    loadChildren                :   './review/review.module#EventReviewPageModule'
}, {
    path                        :   'volunteer',
    loadChildren                :   './volunteer/volunteer.module#EventVolunteerPageModule'
}, {
    path                        :   '',
    redirectTo                  :   'home'
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule
    ],
    declarations: []
})
export class EventModule { }
