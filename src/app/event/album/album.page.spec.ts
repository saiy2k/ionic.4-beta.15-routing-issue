import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventAlbumPage } from './album.page';

describe('EventAlbumPage', () => {
  let component: EventAlbumPage;
  let fixture: ComponentFixture<EventAlbumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAlbumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
